import React, { useState } from 'react';
import { View, StyleSheet, TextInput, Button, Modal } from 'react-native';



const GoalInput = props => {

    const [enteredGoal, setEnteredGoal] = useState('');

    const enteredGoalHandler = (textentered) => {
        setEnteredGoal(textentered);
    }

    return (
        <Modal visible={props.visible} animationType="slide">
            <View style={styles.inputBox}>
                <TextInput placeholder="Enter new goal"
                    style={styles.input} onChangeText={enteredGoalHandler} />
                <View style={styles.buttonBox}>
                    <View style={styles.button}>
                        <Button title="ADD" onPress={() => props.onAddGoal(enteredGoal)} />
                    </View>
                    <View style={styles.button}>
                        <Button title="CANCEL" onPress={ props.onCancelGoal } color="red"/>
                    </View>
                    
                </View>
                
            </View>
        </Modal>
        
    );
}

const styles = StyleSheet.create({
    inputBox: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        alignContent: "center"
    },
    input: {
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
        padding: 10,
        marginBottom: 5
        
    }, 
    buttonBox: {
        flexDirection: "row-reverse",
        justifyContent: "space-around",
        
    },

    button:{
        width: "40%"
    }
});

export default GoalInput;