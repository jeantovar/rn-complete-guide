import React, { useState } from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  Button
} from 'react-native';
import GoalItem from './components/GoalItem';
import GoalInput from './components/GoalInput';

export default function App() {

  const [courseGoals, setCourseGoals] = useState([]);

  const [showGoalBox, setShowGoalBox] = useState(false);

  const courseGoalsHandler = (titleGoal) => {
    if(titleGoal.length === 0){
      return ;
    }
    setCourseGoals(currentGoals => [...currentGoals, {
      key: Math.floor((Math.random() * 10000) + 1),
      value: titleGoal
    }]);
    setShowGoalBox(false);
    //console.log(enteredGoal);
    //console.log(courseGoals);
  }

  const onDeleteHandler = (goalId) => {
    console.log('Touchable action!'+goalId);
    setCourseGoals(courseGoals => {
      
      return courseGoals.filter( goal => goal.key !== goalId );
    });
  }

  const onCancelGoalHandler = () =>{
    setShowGoalBox(false);
  }
  return (
    <View style={styles.screen}>
      <Button title="Add Goals" onPress={()=>setShowGoalBox(true)}/>
      <GoalInput onAddGoal={courseGoalsHandler} 
                  visible={showGoalBox} 
                  onCancelGoal ={ onCancelGoalHandler }/>
      <FlatList
        keyExtractor={(item, index) => item.key.toString()} //no es necesaria, lo hace por defecto asi
        data={courseGoals}
        renderItem={
          itemData => <GoalItem goalId={itemData.item.key}
                        title={itemData.item.value} onDelete={onDeleteHandler} />
        } />

    </View>
  );
}

const styles = StyleSheet.create({
  screen: { padding: 50 }

});


